package empSalary;

public class HourlyEmployee implements Employee {
	private double hoursWorked;
	private double hourlyPay;

	public HourlyEmployee(double hourlyPay, double hoursWorked) {
		this.hourlyPay=hourlyPay;
		this.hoursWorked=hoursWorked;
	}
	public double getHoursWorked() {
		return hoursWorked;
	}
	public double getHourlyPay() {
		return hourlyPay;
	}
	

	@Override
	public double getWeeklyPay() {
		return (this.hourlyPay)*(this.getHoursWorked());
	}
}
