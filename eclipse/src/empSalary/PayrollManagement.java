package empSalary;

public class PayrollManagement {

	public static void main(String[] args) {
		Employee [] employee = new Employee[5];
		employee[0] = new SalariedEmployee(20000);
		employee[1] = new HourlyEmployee(45,18);
		employee[2] = new UnionizedHourlyEmployee(30,1.5,45,18);
		employee[3] = new SalariedEmployee(10000);
		employee[4] = new SalariedEmployee(50000);
		
		System.out.println(getTotalExpenses(employee));
	}

	public static double getTotalExpenses(Employee[] employee) {
		double expenses = 0;
		for (int i = 0; i<employee.length; i++) {
			expenses += employee[i].getWeeklyPay();
		}
		return expenses;

	}

}
