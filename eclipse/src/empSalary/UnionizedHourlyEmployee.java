package empSalary;

public class UnionizedHourlyEmployee extends HourlyEmployee{
	private double maxHoursPerWeek;
	private double overtimeRate;
	
	public UnionizedHourlyEmployee(double hoursWorked, double hourlyPay, double maxHour, double overtime) {
		super(hoursWorked,hourlyPay);
		this.maxHoursPerWeek = maxHour;
		this.overtimeRate = overtime;
	}
	
	

	public double getWeeklyPay() {
		
		if(getHoursWorked() <= maxHoursPerWeek)
		{
			return getHoursWorked()*getHourlyPay();
		}
		else 
		{
			return getHoursWorked()*getHourlyPay()+(this.maxHoursPerWeek*this.overtimeRate);
		}
		
	}
}
