package empSalary;

public class SalariedEmployee implements Employee {
	private double yearlySalary;
	
	public SalariedEmployee (double yearlySalary) {
		this.yearlySalary = yearlySalary;
	}
	
	public double getYearlySalaray() {
		return this.yearlySalary;
	}
	
	@Override
	public double getWeeklyPay() {
		return this.yearlySalary = this.yearlySalary/52;
	}

}
