package milkyway;


import java.util.Collection;

public class CollectionMethods {
	public static Collection<Planet>getLargerThan(Collection<Planet>planets,double size){
		Object[] p = planets.toArray();
		for (int i = 0; i<p.length;i ++) {
			if(((Planet) p[i]).getRadius() >= size ) {
				return planets;
			}
		}
		return null;
	}
}
