package milkyway;

import java.util.Objects;

/**
 * A class to store information about a planet
 * @author Daniel
 *
 */

public class Planet {
	/**
	  * The name of the solar system the planet is contained within
	  */
	 private String planetarySystemName;
	 
	 /**
	  * The name of the planet.
	  */
	 private String name;
	 
	 /**
	  * From inside to outside, what order is the planet. (e.g. Mercury = 1, Venus = 2, etc)
	  */
	 private int order;
	 
	 /**
	  * The size of the radius in Kilometers.
	  */
	 private double radius;

	 /**
	  * A constructor that initializes the Planet object
	  * @param planetarySystemName The name of the system that the planet is a part of
	  * @param name The name of the planet
	  * @param order The order from inside to out of how close to the center the planet is
	  * @param radius The radius of the planet in kilometers
	  */
	 public Planet(String planetarySystemName, String name, int order, double radius) {
	  this.planetarySystemName = planetarySystemName;
	  this.name = name;
	  this.order = order;
	  this.radius = radius;
	 }
	 
	 /**
	  * @return The name of the planetary system (e.g. "the solar system")
	  */
	 public String getPlanetarySystemName() {
	  return planetarySystemName;
	 }
	 
	 /**
	  * @return The name of the planet (e.g. Earth or Venus)
	  */
	 public String getName() {
	  return name;
	 }
	 
	 /**
	  * @return The rank of the planetary system
	  */
	 public int getOrder() {
	  return order;
	 }


	 /**
	  * @return The radius of the planet in question.
	  */
	 public double getRadius() {
	  return radius;
	 }
	 
	 public int sortPLanet(Planet planet) {
		 
		int sortPlanet = this.getName().compareTo(planet.getName());
		if (sortPlanet == 0)
		{
			return (int)planet.radius - (int)this.radius ;
		}
		return sortPlanet;
	 }
		
	 @Override
		public boolean equals( Object other)
		{
			if (this==other)
			{
				return true;
			}
			if(other==null)
			{
				return false;
			}
			if(this.getPlanetarySystemName()!=((Planet) other).getPlanetarySystemName())
			{
				return false;
			}
			Planet planet = (Planet)other;
			if(!this.name.equals(planet.name))
			{
				return false;
			}
			return true;
		}
	 @Override
	 public int hashCode() {
		String combined = this.name + this.planetarySystemName;
		return combined.hashCode();
	 }
	 
	}

